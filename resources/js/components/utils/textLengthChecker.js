export function textLengthChecker(event, limit)
{
    const value = event.target.value;

    if (value.length == limit) {
        event.preventDefault();
    }
}