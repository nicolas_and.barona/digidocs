export function isEmpty(value) {

    return [null, "", undefined, 0].includes(value);
}