import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router ({
    routes: [
        { path: '/',
          name: 'login',
          component: require('./components/modulos/authenticate/login').default
        },
        { path: '/registro',
          name: 'registro',
          component: require('./components/modulos/authenticate/registro').default
        },
        { path: '/registro/alcalde', 
          component: require('./components/modulos/authenticate/registros/alcalde').default
        },
        { path: '/registro/sec_despacho', 
          component: require('./components/modulos/authenticate/registros/sec_despacho').default
        },
        { path: '/registro/aux_secretaria', 
          component: require('./components/modulos/authenticate/registros/aux_secretaria').default
        },
        { path: '/registro/archivo', 
          component: require('./components/modulos/authenticate/registros/archivo').default
        },
        { path: '/registro/rector', 
          component: require('./components/modulos/authenticate/registros/rector').default
        },
        { path: '/registro/sec_rectoria', 
          component: require('./components/modulos/authenticate/registros/sec_rectoria').default
        },
        {
          path: '/enviar-email',
          name: 'enviarEmail',
          component: require('./components/modulos/dashboard/usuario/enviarEmail').default
        },
        {
          path: '/recuperar-contrasena/:codigo',
          name: 'recuperarContrasena',
          component: require('./components/modulos/dashboard/usuario/recuperarContrasena').default
        },

        //usuario
        { path: '/dashboard',
          name:'dashboard.index', 
          component: require('./components/modulos/dashboard/index').default,
          meta: {
            auth: true,
            user: true
          }
        },

        //admin
        { path: '/admin_dashboard',
          name:'dashboard.indexAdmin', 
          component: require('./components/modulos/dashboard/admin/indexAdmin').default,
          meta: {
            auth: true,
            admin: true
          }
        }
    ],
    mode: 'history',
    base: '/'
})

export default router;


const ROL_ADMIN = 'admin';
const ROL_USER = 'user';

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.auth)) {

    const usuario = localStorage.getItem('usuario');

    if (usuario) {
      next();
    } else {
      next({
        name: 'login'
      });
    }
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.admin)) {

    const rol = localStorage.getItem('rol');

    if (rol == ROL_ADMIN) {
      next();
    } else {
      next({
        name: 'login'
      });
    }
  } else {
    next();
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.user)) {

    const rol = localStorage.getItem('rol');

    if (rol == ROL_USER) {
      next();
    } else {
      next({
        name: 'login'
      });
    }
  } else {
    next();
  }
});

