<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="url" content="{{ url('') }}">
    <link rel="icon" href="{{ asset('img/logo.png') }}" type="image/ico" />
    <title>DigiDocs</title>
    <link rel="stylesheet" href="{{ asset('css/plantilla.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <div id="app">
        @if(Auth::check())
        <Admin ruta="{{route('basepath')}}" :usuario="{{ Auth::user() }}"></Admin>
        @else
        <Auth ruta="{{route('basepath')}}"></Auth> 
        @endif
    </div>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/plantilla.js') }}"></script>
</body>
</html>