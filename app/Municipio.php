<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    public $allowedSorts = [];
    protected $perPage = 1000000;

    public function scopeIdDepartamento(Builder $query, $value)
    {
        $query->where('departamento_id', $value);
    }
}
