<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Archivo;
use Illuminate\Support\Facades\Storage;

class ArchivoController extends Controller
{
    public function store(Request $formData)
    {   
        // $max_size = (int) ini_get('upload_max_filesize') * 1024;

        $request = new Request(json_decode($formData->params, true));
        $numberFiles = $request->numberFiles;

        if ($numberFiles > 0) {

            for ($i=0; $i < $numberFiles; $i++) { 

                $key = 'file-'.$i;
                $file = $formData->file($key);
                $fileName = $file->getClientOriginalName();
                $path = 'usuario_'.$request->id_usuario.'/'.$request->carpeta.'/'.$fileName;

                $archivo = new Archivo;  
                $archivo->nombre        = $fileName;
                $archivo->url_ubicacion = $path;
                $archivo->peso          = $_FILES[$key]['size'];
                $archivo->tipo          = $_FILES[$key]['type'];
                $archivo->id_carpeta    = $request->id_carpeta;
                $archivo->save();

                Storage::disk('s3')->put($path, file_get_contents($file));
            }

            return Response::created("Archivo", "Sus archivos se han cargado exitosamente", $numberFiles);
        }
    }

    public function destroy($id) 
    {
        $archivo = Archivo::findOrFail($id);
        
        Storage::disk('s3')->delete($archivo->url_ubicacion);
        $archivo->delete();

        return Response::noContent("Archivo", "Archivo eliminado");
    }

    public function download($id) 
    {
        $archivo = Archivo::findOrFail($id);
        $headers = [
            'Content-Type'        => 'application/octet-stream',
            'Content-Disposition' => 'attachment; filename="'. $archivo->nombre .'"',
        ];
        return \Response::make(Storage::disk('s3')->get($archivo->url_ubicacion), 200, $headers);

        
    }
}
