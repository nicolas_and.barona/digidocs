<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ActualizarEstado;
use Illuminate\Support\Facades\Mail;
use App\Usuario;
use App\Entidad;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UsuarioController extends Controller
{
    public function usuarios(Request $request) {
        $usuarios = Usuario::all();
        foreach ($usuarios as $key => $value) {
            $entidad = Entidad::where( 'id_entidad', $value['id_entidad'])->get();
            $value->data_usuario = $entidad;
        }
        return response()->json($usuarios);
    }

    public function update_entidad(Request $request) {
       
        $usuario = Usuario::where('id_usuario', $request->input('id_usuario'))->first(); 
        $entidad = Entidad::where('id_entidad', $request->input('id_entidad'))->first(); 

        $usuario->nombres_usuario = $request->input('usuario_nombre');
        $usuario->apellidos_usuario = $request->input('usuario_apellido');
        $usuario->cedula_usuario = $request->input('usuario_documento');
        $usuario->email = $request->input('usuario_email');
        $usuario->telefono_usuario = $request->input('usuario_telefono');
        $usuario->save();

        $entidad->nombre_entidad = $request->input('entidad_nombre');
        $entidad->telefono_entidad = $request->input('entidad_telefono');
        $entidad->direccion_entidad = $request->input('entidad_direccion');
        $entidad->save();

        return response()->json([
            'mensaje' => 'Se actualizaron los datos correctamente'
        ], 200);
    }

    public function updateEstado(Request $request, $id) {
        $estado = Usuario::where('id_usuario', $id)->first(); 
        $entidad = Entidad::where('id_entidad', $estado->id_entidad)->first();

        $estado->id_estado = 2;
        $entidad->id_estado = 2;
         if ($estado->save() && $entidad->save() ) {
             return response()->json(['status' => true, 'message' => 'Se actualizo usuario']);
             $this->estadoMail($estado);
        } else {
             return response()->json(['status' => false, 'message' => 'No se pudo actualizar usuario ']);
        }
        
    }

    public function estadoMail($estado) {
        if($estado['id_estado'] === 2) {
            $datos = [
                'usuario_name' => $estado['nombres_usuario'].' '.$estado['apellidos_usuario']
            ];
    
            Mail::to($estado['email'])->send(new ActualizarEstado($datos));
        }
    }

    public function updateEstadoSuspender(Request $request, $id) {
        $estado = Usuario::where('id_usuario', $id)->first(); 
        $estado->id_estado = 3;
         if ($estado->save()) {
             return response()->json(['status' => true, 'message' => 'Se actualizo usuario']);
        } else {
             return response()->json(['status' => false, 'message' => 'No se pudo actualizar usuario ']);
        }
    }

    public function eliminarUser(Request $request, $id) {
        $estado = Usuario::where('id_usuario', $id)->first(); 
        $estado->id_estado = 4;
         if ($estado->save()) {
             return response()->json(['status' => true, 'message' => 'Se actualizo usuario']);
        } else {
             return response()->json(['status' => false, 'message' => 'No se pudo actualizar usuario ']);
        }
    }

    public function store(Request $request) 
    {
        $validator = Validator::make($request->all(), $this->rules(), $this->messages());

        if ($validator->fails())
        {
            return Response::badRequest("Usuario", $validator->errors());
        }

        $this->createUser($request);

        return Response::created("Usuario", "Registro exitoso", null);
    }

    public function validateCreateRequest($values)
    {
        return Validator::make($values, $this->rulesToCreate(), $this->messages());
    }

    public function createUser(Request $request) 
    {
        $user = new Usuario();
        $user->cedula_usuario    = $request->cedula_usuario;
        $user->nombres_usuario   = $request->nombres_usuario;
        $user->apellidos_usuario = $request->apellidos_usuario;
        $user->telefono_usuario  = $request->telefono_usuario;
        $user->email             = $request->email;
        $user->password          = Hash::make($request->password);
        $user->id_estado         = $request->id_estado;
        $user->id_entidad        = $request->id_entidad;
        $user->save();
    }

    public function rules()
    {
        $rules = $this->rulesToCreate();
        $rules['id_entidad'] = "required|numeric|min:1";

        return $rules;
    }

    private function rulesToCreate()
    {
        return [
            'cedula_usuario'    => 'required|unique:usuarios',
            'nombres_usuario'   => 'required',
            'apellidos_usuario' => 'required',
            'telefono_usuario'  => 'required|max:10|regex:/^[0-9]+$/',
            'email'             => 'required|email|unique:usuarios',
            'password'          => 'required',
            'id_estado'         => 'required|numeric|min:1'
        ];
    }

    private function messages()
    {
        return [
            'cedula_usuario.required'    => 'Cedula es requerido',
            'cedula_usuario.unique'      => 'Cedula existente',
            'nombres_usuario.required'   => 'Nombres es requerido',
            'apellidos_usuario.required' => 'Apellidos es requerido',
            'telefono_usuario.required'  => 'Telefono es requerido',
            'telefono_usuario.regex'     => 'Sólo números',
            'telefono_usuario.max'       => 'Máximo :max caracteres',
            'email.required'             => 'Correo es requerido',
            'email.email'                => 'Correo inválido',
            'email.unique'               => 'Correo existente',
            'password.required'          => 'Password es requerido',
            'id_estado.required'         => 'Estado es requerido',
            'id_estado.numeric'          => 'Estado debe ser un número',
            'id_estado.min'              => 'Estado es requerido',
            'id_entidad.required'        => 'Entidad es requerido',
            'id_entidad.numeric'         => 'Entidad debe ser un número',
            'id_entidad.min'             => 'Entidad es requerido',
        ];
    }
}
