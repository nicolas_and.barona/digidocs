<?php

namespace App\Http\Controllers;

use App\Mail\RecuperarContrasena;
use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class RecuperarContrasenaController extends Controller
{
    public function enviarEmail(Request $request) {

        $user = Usuario::firstWhere('email', $request->email);

        if ($user == null) {

            return response()->json([
                'mensaje' => 'Email no existe en nuestros registros'
            ], 400);
        }

        $user->codigo_recuperar_password = Str::random(20);
        $user->save();
        
        $datos = [
            'link' => env('APP_VUE_URL').'/recuperar-contrasena/'.$user->codigo_recuperar_password,
            'usuario_name' => $user['nombres_usuario'].' '.$user['apellidos_usuario']
        ];

        Mail::to($user->email)->send(new RecuperarContrasena($datos));

        return response()->json([
            'mensaje' => 'Se ha enviado un mensaje a tu correo electrónico'
        ], 200);
    }

    public function verificarCodigo(Request $request) {

        $user = Usuario::firstWhere('codigo_recuperar_password', $request->codigo);
        $valido = $user != null ? true : false;

        return response()->json([
            'valido' => $valido
        ], 200);
    }

    public function guardarNuevaContrasena(Request $request) {

        $user = Usuario::firstWhere('email', $request->email);

        if ($user == null) {

            return response()->json([
                'mensaje' => 'Email no existe en nuestros registros'
            ], 400);
        }

        if ($user->codigo_recuperar_password) {

            $user->password = Hash::make($request->password);
            $user->codigo_recuperar_password = "";
            $user->save();

            return response()->json([
                'mensaje' => 'Se ha guardado tu nueva contraseña'
            ], 200);
        }

        return response()->json([
            'mensaje' => 'La operación no es válida'
        ], 400);
    }
}
