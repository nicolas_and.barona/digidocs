<?php

namespace App\Http\Controllers;

class Response
{
    public static function ok($type, $data, $pages = 0, $message = "Ok")
    {
        return response()->json([
            'type'    => $type,
            'message' => $message,
            'data'    => $data,
            'pages'   => $pages,
            'errors'  => []
        ], 200);
    }

    public static function created($type, $message, $data)
    {
        return response()->json([
            'type'    => $type,
            'message' => $message,
            'data'    => $data,
            'pages'   => 0,
            'errors'  => []
        ], 201);
    }

    public static function badRequest($type, $errors)
    {
        return response()->json([
            'type'    => $type,
            'message' => 'Error en la solicitud',
            'data'    => null,
            'pages'   => 0,
            'errors'  => $errors
        ], 400);
    }

    public static function unauthorized($errors)
    {
        return response()->json([
            'type'    => "",
            'message' => 'No autorizado',
            'data'    => null,
            'pages'   => 0,
            'errors'  => $errors
        ], 403);
    }

    public static function noContent($type, $message)
    {
        return response()->json([
            'type'    => $type,
            'message' => $message,
            'data'    => null,
            'pages'   => 0,
            'errors'  => []
        ], 200);
    }
}
