<?php

namespace App\Http\Controllers;

use App\Entidad;
use App\Http\Resources\EntidadResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EntidadController extends Controller
{
    public function index() 
    {
        $entidades = Entidad::applyFilters()->applySorts()->jsonPaginate();

        return Response::ok("Entidad", EntidadResource::collection($entidades));
    }

    public function store(Request $request) 
    {
        $newRequest = new Request(json_decode($request->params, true));

        $validator = $this->validateCreateRequest($newRequest->all());

        if ($validator->fails())
        {
            return Response::badRequest("Entidad", $validator->errors());
        }

        $usuarioController = new UsuarioController();
        $validatorUser = $usuarioController->validateCreateRequest($newRequest->all());

        if ($validatorUser->fails())
        {
            return Response::badRequest("Usuario", $validator->errors());
        }

        $file = $request->file('escudo');

        if ($file) {

            $newRequest->merge(['escudo_entidad' => $file->getClientOriginalName()]);
        }   

        if($bdEntidad =  Entidad::where('nit_entidad', $newRequest->nit_entidad)->first()) {
            return response()->json([
                'message' => 'Usuario ya existe en nuestros registros'
            ]);
        }else {
            $entidad = $this->createEntidad($newRequest);
    
            if ($entidad->id_entidad) {
    
                $usuarioCreateRequest = [
                    'cedula_usuario'    => $newRequest->cedula_usuario,
                    'nombres_usuario'   => $newRequest->nombres_usuario,
                    'apellidos_usuario' => $newRequest->apellidos_usuario,
                    'telefono_usuario'  => $newRequest->telefono_usuario,
                    'email'             => $newRequest->email,
                    'password'          => $newRequest->password,
                    'id_estado'         => $newRequest->id_estado,
                    'id_entidad'        => $entidad->id_entidad
                ];
        
                $usuarioController->createUser(new Request($usuarioCreateRequest));
                
                return Response::created("Usuario", "Registro exitoso", null);
            }   

        }

        return response()->json([
            'message' => 'Ocurrió un error inesperado'
        ], 500);
    }

    public function validateCreateRequest($values)
    {
        return Validator::make($values, $this->rulesToCreate(), $this->messages());
    }

    public function createEntidad(Request $request)
    {
        $entidad = new Entidad();
        $entidad->id_departamento   = $request->id_departamento;
        $entidad->id_municipio      = $request->id_municipio;
        $entidad->dane              = $request->dane;
        $entidad->nit_entidad       = $request->nit_entidad;
        $entidad->nombre_entidad    = $request->nombre_entidad;
        $entidad->telefono_entidad  = $request->telefono_entidad;
        $entidad->direccion_entidad = $request->direccion_entidad;
        $entidad->correo_entidad    = $request->correo_entidad;
        $entidad->escudo_entidad    = $request->escudo_entidad;
        $entidad->id_estado         = $request->id_estado;
        $entidad->save();

        return $entidad;
    }

    private function rulesToCreate()
    {
        return [
            'id_departamento'   => 'required|numeric|min:1',
            'id_municipio'      => 'required|numeric|min:1',
            'dane'              => 'nullable',
            'nit_entidad'       => 'required',
            'nombre_entidad'    => 'required',
            'telefono_entidad'  => 'required|max:10|regex:/^[0-9]+$/',
            'direccion_entidad' => 'required',
            'correo_entidad'    => 'required|email|unique:entidades',
            'escudo_entidad'    => 'nullable',
            'id_estado'         => 'required|numeric|min:1',
        ];
    }

    private function messages()
    {
        return [
            'id_departamento.required'   => 'Departamento es requerido',
            'id_departamento.numeric'    => 'Departamento debe ser un número',
            'id_departamento.min'        => 'Departamento es requerido',
            'id_municipio.required'      => 'Municipio es requerido',
            'id_municipio.numeric'       => 'Municipio debe ser un número',
            'id_municipio.min'           => 'Municipio es requerido',
            'nit_entidad.required'       => 'Nit es requerido',
            'nit_entidad.numeric'        => 'Nit debe contener sólo números',
            'nombre_entidad.required'    => 'Nombre es requerido',
            'telefono_entidad.required'  => 'Teléfono es requerido',
            'telefono_entidad.max'       => 'Máximo :max caracteres',
            'telefono_entidad.regex'     => 'Sólo números',
            'direccion_entidad.required' => 'Dirección es requerido',
            'correo_entidad.required'    => 'Correo es requerido',
            'correo_entidad.email'       => 'Correo inválido',
            'correo_entidad.unique'      => 'Correo existente',
            'id_estado.required'         => 'Estado es requerido',
            'id_estado.numeric'          => 'Estado debe ser un número',
            'id_estado.min'              => 'Estado es requerido',
        ];
    }
}
