<?php

namespace App\Http\Controllers;

use App\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function login(Request $request) 
    {
        $user = Usuario::firstWhere('cedula_usuario', $request->cedula_usuario);
        if($user->id_estado == 1) {
            return response()->json([
                'mensaje' => 'Usuario no esta habilitado, por favor pongase en contacto con el administrador'
            ], 400);
        }

        if (!$user) 
        {
            return response()->json([
                'mensaje' => 'Usuario no existe en nuestros registros'
            ], 400);
        }

        if (Hash::check($request->password, $user->password)) 
        {
            return response()->json([
                'authUser' => $user,
                'mensaje' => 'Autenticación correcta'
            ], 200);

        } else {
            
            return response()->json([
                'mensaje' => 'Contraseña incorrecta'
            ], 400);
        }
    }
}
