<?php

namespace App\Http\Controllers;

use App\Http\Resources\MunicipioResource;
use App\Municipio;

class MunicipioController extends Controller
{
    public function index()
    {
        $municipios = Municipio::applyFilters()->applySorts()->jsonPaginate();

        return Response::ok(
            "Municipio", MunicipioResource::collection($municipios)
        );
    }
}
