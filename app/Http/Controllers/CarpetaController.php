<?php

namespace App\Http\Controllers;
use App\Carpeta;
use App\Http\Resources\CarpetaResource;
use Illuminate\Http\Request;

class CarpetaController extends Controller
{
    public function index()
    {
        $carpetas = Carpeta::applyFilters()->applySorts()->jsonPaginate();

        return Response::ok("Carpetas", CarpetaResource::collection($carpetas));
    }

    public function carpeta_by_id(Request $request) {
        $folder = Carpeta::firstWhere('id_carpeta', $request->id);
        return response()->json(
            array(
                'status' => 'success',
                'carpeta' => $folder
            )
        ); 
    }

    public function update_carpeta(Request $request) {
        $folder = Carpeta::where('id_carpeta', $request->id)->first();

        $folder->nombre = $request->nombre;
        $folder->save();
        
        return response()->json([
            'mensaje' => 'Se ha actualizado la carpeta'
        ], 200);
    }

    public function store(Request $request) 
    {
        $carpetaRepetida = Carpeta::where('id_usuario', $request->id_usuario)
            ->where('nombre', $request->nombre)
            ->get()
            ->first();

        if ($carpetaRepetida) {

            return Response::badRequest("Carpeta", [
                "nombre" => [
                    'Carpeta existente'
                ]
            ]);
        }

        $carpeta = new Carpeta;
        $carpeta->nombre     = $request->nombre;
        $carpeta->id_usuario = $request->id_usuario;
        $carpeta->save();

        //crear carpeta en directorio

        return Response::created("Carpeta", "La carpeta se ha creado", $carpeta);
    }
}
