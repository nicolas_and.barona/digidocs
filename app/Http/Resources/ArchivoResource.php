<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArchivoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_archivo'    => $this->id,
            'id_carpeta'    => $this->id_carpeta,
            'nombre'        => $this->nombre,
            'url_ubicacion' => $this->url_ubicacion,
            'peso'          => $this->peso,
            'tipo'          => $this->tipo,
            'created_at'    => $this->created_at->format('Y-m-d')
        ];
    }
}
