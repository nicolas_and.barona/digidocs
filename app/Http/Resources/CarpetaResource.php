<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarpetaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_carpeta' => $this->id_carpeta,
            'nombre'     => $this->nombre,
            'id_usuario' => $this->id_usuario,
            'archivos'   => ArchivoResource::collection($this->archivos)
        ];
    }
}
