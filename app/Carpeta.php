<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Carpeta extends Model
{
    public $allowedSorts = [];
    protected $perPage = 1000000;
    protected $primaryKey = 'id_carpeta';

    public function scopeIdUsuario(Builder $query, $value)
    {
        $query->where('id_usuario', $value);
    }

    public function archivos()
    {
        return $this->hasMany(Archivo::class, 'id_carpeta', 'id_carpeta');
    }

}
