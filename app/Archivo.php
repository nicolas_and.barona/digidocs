<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    public $allowedSorts = [];
    protected $perPage = 1000000;

    public function scopeIdCarpeta(Builder $query, $value)
    {
        $query->where('id_carpeta', $value);
    }
}
