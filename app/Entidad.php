<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Entidad extends Model
{
    protected $table = 'entidades';
    protected $primaryKey = 'id_entidad';

    public $allowedSorts = [];
    protected $perPage = 1000000;

    public function scopeIdMunicipio(Builder $query, $value)
    {
        $query->where('id_municipio', $value);
    }

    public function scopeIdEstado(Builder $query, $value)
    {
        $query->where('id_estado', $value);
    }
}
