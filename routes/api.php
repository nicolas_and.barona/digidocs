<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','LoginController@login');
Route::post('logout','LoginController@logout');

Route::resource('carpetas','CarpetaController');
Route::resource('archivos','ArchivoController');
Route::resource('departamentos', 'DepartamentoController');
Route::resource('municipios', 'MunicipioController');
Route::resource('entidades', 'EntidadController');
Route::resource('usuarios', 'UsuarioController');

Route::get('archivos/download/{id}', 'ArchivoController@download');
Route::post('carpeta-by-id', 'CarpetaController@carpeta_by_id');
Route::post('update-carpeta', 'CarpetaController@update_carpeta');
Route::post('enviar-email', 'RecuperarContrasenaController@enviarEmail');
Route::post('verificar-codigo', 'RecuperarContrasenaController@verificarCodigo');
Route::post('guardar-nueva-contrasena', 'RecuperarContrasenaController@guardarNuevaContrasena');

//Admin
Route::get('get_usuarios', 'UsuarioController@usuarios');
Route::post('update_entidad', 'UsuarioController@update_entidad');
Route::post('cambiar/{id}', 'UsuarioController@updateEstado');
Route::post('suspender/{id}', 'UsuarioController@updateEstadoSuspender');
Route::post('eliminar/{id}', 'UsuarioController@eliminarUser');